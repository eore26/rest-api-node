let fs = require("fs");

// let readDir = (dir) => {
//   if (dir === null) {

//   }
//   fs.readdir(dir, (err, files) => {
//     files.filter(el => el !== "route.js").forEach(el => {

//       readDir(`${dir}/${el}`)
//       fs.readdir(`./core/api/${el}`, (err, ))
//     })
//   });
// }

let route = router => {
  fs.readdir("./core/api", (err, folder) => {
    folder.filter(el => el !== "route.js").forEach(el => {
      fs.readdir(`./core/api/${el}`, (err, files) => {
        files.forEach(elFiles => {
          router.use(`/${el}`, require(`./${el}/${elFiles}`)(router));
        });
      });
    });
  });
  // router.use("/user", require("./user/addUser")(router));
  // router.use("/user", require("./user/findUser")(router));

  return router;
};

module.exports = route;
