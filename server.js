let express = require("express");
let app = express();
let router = express.Router();

let API = require("./core/api/route");

let { port } = require("./configs/server");

app.use("/api", API(router));

// app.get('/', (req,res) => {req.connection.remoteAddress.})

app.listen(port, () => console.log(`Server up di port ${port}`));
