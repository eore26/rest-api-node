let mongodb = require("mongodb");
let { host, port, database } = require("../configs/database");
mongodb.MongoClient(`mongodb://${this.host}:${this.port}/${this.database}`);
class Connection {
  constructor(collection) {
    this.host = host;
    this.port = port;
    this.database = database;
    this.collection = collection;
    this.connect();
    // this.connection = this.connect();
    // this.connection = () => {
    //   return new Promise((resolve, reject) => {
    //     let con = new mongodb(
    //       `mongodb://${this.host}:${this.port}/${this.database}`,
    //       { useNewUrlParser: true }
    //     );
    //     con.db().
    //     mongodb.connect(
    //       `mongodb://${this.host}:${this.port}/${this.database}`,
    //       { useNewUrlParser: true },
    //       (err, db) => {
    //         resolve(db.db(this.database).collection(this.collection));
    //       }
    //     );
    //   });
    // };
  }

  init() {
    let client = new mongodb.MongoClient(
      new mongodb.Server(this.host, this.port),
      { native_parser: true }
    );
    client.connect();
    return client;
  }

  connect() {
    mongodb.MongoClient.connect(
      `mongodb://${this.host}:${this.port}/${this.database}`,
      { useNewUrlParser: true },
      (err, db) => {
        this.connection = db.db(this.database).collection(this.collection);
      }
    );
  }

  insert(model) {
    this.connection.insert(model);
  }

  update(where, model) {
    this.connection.updateOne(where, model);
  }
}

module.exports = Connection;
